import _ from 'lodash'

export default class User {
  static get (id) {
    let storage = JSON.parse(localStorage.getItem('users'))

    let userEdit = ''
    userEdit = _.findIndex(storage, e => {
      return e.id === parseInt(id)
    })

    return storage[userEdit]
  }

  static edit (user) {
    let storage = JSON.parse(localStorage.getItem('users'))

    let userEdit = ''
    userEdit = _.findIndex(storage, e => {
      return e.id === parseInt(user.id)
    })

    storage[userEdit] = user
    localStorage.setItem('users', JSON.stringify(storage))
  }

  static add (user) {
    let storage = JSON.parse(localStorage.getItem('users'))
    user['id'] = Math.floor(Math.random() * 100)

    if (storage === 'undefined' || storage === null) {
      storage = localStorage.setItem('users', JSON.stringify([]))
    }

    storage.push(user)
    localStorage.setItem('users', JSON.stringify(storage))
  }

  static delete (id) {
    let storage = JSON.parse(localStorage.getItem('users'))

    if (storage.length === 1) {
      storage = []
      localStorage.setItem('users', JSON.stringify(storage))
    } else {
      let removed = _.remove(storage, item => {
        return item.id !== parseInt(id)
      })
      storage = removed
    }

    localStorage.setItem('users', JSON.stringify(storage))
  }
}
