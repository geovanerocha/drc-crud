const getParam = (route, param) => {
  return route.split(`${param}=`)[1]
}

const getHash = route => {
  return route.split('?')[0]
}

export { getParam, getHash }
