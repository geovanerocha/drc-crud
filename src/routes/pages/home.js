import Title from '../../components/atoms/Title'
import DataPanel from '../../components/molecules/DataPanel'

const mountTitle = () => {
  const title = Title({
    text: 'dr.consulta - cadastro',
    className: 'main-title'
  })

  return title
}

const mountDataPanel = () => {
  const dataPanel = new DataPanel()
  return dataPanel.renderTemplate()
}

const bootstrapComponents = page => {
  page.appendChild(mountTitle())
  page.appendChild(mountDataPanel())
}

const Home = () => {
  if (!localStorage.getItem('users')) {
    localStorage.setItem('users', JSON.stringify([]))
  }

  const home = document.createElement('main')
  home.setAttribute('class', 'main-content')
  bootstrapComponents(home)

  return home
}

export default Home
