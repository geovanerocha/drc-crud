import Home from './home'

beforeEach(() => {
  let storageMock = () => {
    var storage = {}

    return {
      setItem: (key, value) => {
        storage[key] = value || ''
      },
      getItem: key => {
        return key in storage ? storage[key] : null
      },
      removeItem: key => {
        delete storage[key]
      },
      get length () {
        return Object.keys(storage).length
      },
      key: i => {
        var keys = Object.keys(storage)
        return keys[i] || null
      }
    }
  }

  window.localStorage = storageMock()
})

describe('#Home', () => {
  test('is rendering', () => {
    const home = Home()
    expect(home).toBeDefined()
  })
})
