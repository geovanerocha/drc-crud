import Home from './pages/home'

const ROUTES = {
  'home': {
    name: 'Home',
    render: Home
  }
}

export default ROUTES
