import ROUTES from './routes'
import { getHash } from './parsers'

export default class Router {
  static go (hash = '', params) {
    if (hash.startsWith('#')) hash = hash.replace('#', '')
    if (hash) location.hash = `#${hash}`
    if (params) location.hash += `?id=${params.id}`

    const page = ROUTES[getHash(hash)]

    if (!page) {
      location.hash = ''
      return page.render()
    }

    document.querySelector('.app').innerHTML = ''
    document.querySelector('.app').appendChild(page.render(hash))
  }
}
