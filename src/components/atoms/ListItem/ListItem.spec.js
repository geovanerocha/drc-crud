import ListItem from './ListItem'

describe('#ListItem', () => {
  test('should render', () => {
    const listItem = ListItem({
      id: 32,
      name: 'Dr.Consultinha',
      birthdate: '08/11/1991',
      disease: 'Menino do mock'
    })
    expect(listItem).toBeDefined()
  })

  test('should render edit button', () => {
    const listItem = ListItem({
      id: 32,
      name: 'Dr.Consultinha',
      birthdate: '08/11/1991',
      disease: 'Menino do mock'
    })
    expect(listItem.querySelector('button.edit')).toBeDefined()
  })

  test('should render delete button', () => {
    const listItem = ListItem({
      id: 32,
      name: 'Dr.Consultinha',
      birthdate: '08/11/1991',
      disease: 'Menino do mock'
    })
    expect(listItem.querySelector('button.delete')).toBeDefined()
  })

  test('should render informations', () => {
    const listItem = ListItem({
      id: 32,
      name: 'Dr.Consultinha',
      birthdate: '08/11/1991',
      disease: 'Menino do mock'
    })
    expect(listItem.querySelector('.content-wrapper')).toBeDefined()
  })
})
