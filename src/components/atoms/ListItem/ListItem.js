import Button from '../Button'
import User from '../../../services/User'
import Router from '../../../routes/Router'
import Modal from '../../molecules/Modal'

import './ListItem.scss'

const convertDate = date => {
  date = new Date(date)
  return `${date.getDate() + 1}/${date.getMonth() + 1}/${date.getFullYear()}`
}

const editUser = (id) => {
  const data = User.get(id)
  Modal.open(data)
}

const deleteUser = id => {
  User.delete(id)
  Router.go('home')
}

const ListItem = ({ id, name, birthdate, disease }) => {
  const item = document.createElement('li')
  const contentWrapper = document.createElement('span')

  contentWrapper.className = 'content-wrapper'
  contentWrapper.textContent = `Nome: ${name} - Data de nascimento: ${convertDate(birthdate)} - Doença: ${disease}`

  item.id = id

  item.appendChild(contentWrapper)
  item.appendChild(Button({
    text: 'Editar',
    className: 'edit',
    id,
    onClick: (e) => editUser(e.target.id)
  }))

  item.appendChild(Button({
    text: 'Excluir',
    className: 'delete',
    id,
    onClick: (e) => deleteUser(e.target.id)
  }))

  return item
}

export default ListItem
