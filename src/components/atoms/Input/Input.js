import './Input.scss'

const Input = ({ id = '', type = 'text', name, placeholder = '', className = 'input', required, label = '', value = '' }) => {
  const input = document.createElement('input')
  input.id = id
  input.type = type
  input.name = name
  input.placeholder = placeholder
  input.className = className
  input.required = required
  input.value = value

  return input
}

export default Input
