import Input from './Input'

describe('#Input', () => {
  test('should render', () => {
    const input = Input({ id: 'input' })
    expect(input).toBeDefined()
  })
})
