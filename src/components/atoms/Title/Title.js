import './Title.scss'

const SIZES = {
  big: 'h1',
  medium: 'h2',
  small: 'h3'
}

const Title = ({ size = 'big', text = '', className = '' }) => {
  const title = document.createElement(SIZES[size])
  title.className = `title ${className}`
  title.textContent = text

  return title
}

export default Title
