import Title from './Title'

describe('#Title', () => {
  test('should render', () => {
    const title = Title({
      size: 'medium'
    })
    expect(title).toBeDefined()
  })
})
