import sinon from 'sinon'
import Button from './Button'

describe('#Button', () => {
  test('should render', () => {
    const button = Button({ id: 'btn' })
    expect(button).toBeDefined()
  })

  test('should call onClick', () => {
    const onClick = sinon.spy()
    const button = Button({ id: 'btn', onClick })
    button.click()
    expect(button).toBeDefined()
    expect(onClick.called).toBeTruthy()
  })
})
