import './Button.scss'

const Button = ({ id, type = 'button', text, className = '', onClick }) => {
  const button = document.createElement('button')
  button.id = id || ''
  button.className = `button ${className}`
  button.type = type
  button.textContent = text
  button.onclick = onClick

  return button
}

export default Button
