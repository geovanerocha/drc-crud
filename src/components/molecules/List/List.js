import ListItem from '../../atoms/ListItem'

const List = ({ users }) => {
  const list = document.createElement('ul')
  const hasUser = !!users

  if (hasUser) {
    users.map(user => {
      list.appendChild(ListItem({id: user.id, name: user.name, birthdate: user.birthdate, disease: user.disease}))
    })
  }

  return list
}

export default List
