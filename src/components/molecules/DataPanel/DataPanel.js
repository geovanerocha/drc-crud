import Button from '../../atoms/Button'
import Modal from '../Modal'
import List from '../List'

import './DataPanel.scss'

export default class DataPanel {
  constructor () {
    this.renderTemplate()
  }

  renderTemplate () {
    const dataPanel = document.createElement('section')
    dataPanel.classList.add('data-panel')

    const button = Button({
      type: 'button',
      className: 'new-user',
      text: 'Cadastrar novo Usuário',
      onClick: () => {
        Modal.open()
      }
    })

    let template

    template = document.createElement('p')
    template.className = 'empty'
    template.textContent = 'Você não cadastrou nenhum usuario até o momento'

    const users = JSON.parse(localStorage.getItem('users'))

    if (!!users && users.length > 0) {
      template = List({ users })
    }

    dataPanel.appendChild(template)
    dataPanel.appendChild(button)

    return dataPanel
  }
}
