import DataPanel from './DataPanel'

beforeEach(() => {
  let storageMock = () => {
    var storage = {}

    return {
      setItem: (key, value) => {
        storage[key] = value || ''
      },
      getItem: key => {
        return key in storage ? storage[key] : null
      },
      removeItem: key => {
        delete storage[key]
      },
      get length () {
        return Object.keys(storage).length
      },
      key: i => {
        var keys = Object.keys(storage)
        return keys[i] || null
      }
    }
  }

  window.localStorage = storageMock()
})

describe('#DataPanel', () => {
  test('should render', () => {
    const dataPanel = new DataPanel()
    expect(dataPanel).toBeDefined()
  })
})
