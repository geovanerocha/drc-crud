import Fields from './mock/fields.json'
import Input from '../../atoms/Input'
import Button from '../../atoms/Button'
import Modal from '../../molecules/Modal'
import User from '../../../services/User'
import Router from '../../../routes/Router'

import './Form.scss'

const Form = data => {
  const form = document.createElement('form')
  let hasData = !!data

  if (hasData) {
    form.id = data.id
  }

  Fields.map((field, idx) => {
    return form.appendChild(Input({
      id: field.id,
      name: field.id,
      type: field.type,
      className: `input ${field.className}`,
      placeholder: field.placeholder,
      required: field.required,
      value: (data) ? data[field.id] : null
    }))
  })

  form.onsubmit = (e) => {
    e.preventDefault()
  }

  form.appendChild(Button({
    type: 'submit',
    text: 'Gravar',
    onClick: submitHandler
  }))

  return form
}

const submitHandler = event => {
  event.preventDefault()

  const form = event.target.parentNode
  let validForm = true

  validForm = form.checkValidity()

  if (validForm === true) {
    Modal.close()

    let data = {
      name: form.elements.name.value,
      birthdate: form.elements.birthdate.value,
      disease: form.elements.disease.value
    }

    let hasForm = !!form.id

    if (hasForm) {
      data.id = parseInt(form.id)
      User.edit(data)
      Router.go('home')
      return
    }

    User.add(data)
    Router.go('home')
  }
}

export default Form
