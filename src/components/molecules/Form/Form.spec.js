import Form from './Form'

describe('#Form', () => {
  test('should render', () => {
    const form = Form()
    expect(form).toBeDefined()
  })

  test('should render fields', () => {
    const form = Form()
    expect(form.querySelector('input')).toBeDefined()
  })
})
