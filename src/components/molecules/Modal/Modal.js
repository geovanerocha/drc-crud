import './Modal.scss'

import Form from '../Form'

export default class Modal {
  static open (data) {
    this.modal = document.createElement('div')
    this.modal.className = 'modal'
    this.modal.classList.add('open')

    const closeButton = document.createElement('span')

    closeButton.textContent = 'Esc ou X'
    closeButton.className = 'close'
    closeButton.onclick = () => {
      Modal.close()
    }

    this.modal.appendChild(Form(data))

    this.modal.appendChild(closeButton)

    document.body.appendChild(this.modal)
    document.querySelector('.main-content').classList.add('blur')
    document.addEventListener('keyup', this.closeTrigger)

    return this.modal
  }

  static closeTrigger (event) {
    if (event.code === 'Escape') {
      return Modal.close()
    }
  }

  static close () {
    this.modal.classList.remove('open')
    document.body.removeChild(this.modal)
    document.querySelector('.main-content').classList.remove('blur')
    document.removeEventListener('keyup', this.closeTrigger)
  }
}
