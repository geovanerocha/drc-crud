import Modal from './Modal'

beforeEach(() => {
  let container = document.createElement('div')
  container.className = 'main-content'
  document.body.appendChild(container)
})

describe('#Modal', () => {
  test('should render', () => {
    const modal = Modal.open()
    expect(modal).toBeDefined()
  })

  test('should render form', () => {
    const modal = Modal.open()
    expect(modal.querySelector('form')).toBeDefined()
  })

  test('should close', () => {
    const modal = Modal.close()
    expect(modal).toBeUndefined()
  })
})
